name := "scala-rest"

version := "0.1"

scalaVersion := "2.12.4"

//lazy val root = (project in file(".")).enablePlugins(PlayScala)

lazy val admin = (project in file("modules/admin")).enablePlugins(PlayScala)

lazy val main = (project in file("."))
  .enablePlugins(PlayScala).dependsOn(admin).aggregate(admin)

libraryDependencies ++= Seq(
  jdbc,
  ws,
  guice,
  ws
)