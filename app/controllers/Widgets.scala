package controllers

import javax.inject.Inject
import play.api.Configuration
import play.api.libs.json._
import play.api.libs.ws.{WSClient, WSRequest}
import play.api.mvc._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class Widgets @Inject()(ws: WSClient, components: ControllerComponents, config: Configuration) extends AbstractController(components) {

  def forecasts(option: String): Action[AnyContent] = Action.async {
    val city = config.get[String]("accuweather.city.code")
    val apiKey = config.get[String]("accuweather.api.key")

    val url = f"http://dataservice.accuweather.com/forecasts/v1/daily/5day/$city?apikey=$apiKey&language=vi-vn&details=false&metric=true"
    val request: WSRequest = ws.url(url)

    val futureResponse: Future[JsValue] = request.get().map {
      response =>
        option match {
          case "tomorrow" => (response.json \ "DailyForecasts") (1)
          case _ => (response.json \ "DailyForecasts") (0)
        }
    }

    futureResponse map { x =>
      val result: JsValue = Json.obj(
        "max_temp" -> (x \ "Temperature" \ "Maximum" \ "Value").as[BigDecimal],
        "min_temp" -> (x \ "Temperature" \ "Minimum" \ "Value").as[BigDecimal],
        "desc" -> (x \ "Day" \ "IconPhrase").as[String]
      )
      Ok(result)
    }
  }

  def getForecasts(option: String): Future[JsValue] = {
    val city = config.get[String]("accuweather.city.code")
    val apiKey = config.get[String]("accuweather.api.key")

    val url = f"http://dataservice.accuweather.com/forecasts/v1/daily/5day/$city?apikey=$apiKey&language=vi-vn&details=false&metric=true"
    val request: WSRequest = ws.url(url)

    val futureResponse: Future[JsValue] = request.get().map {
      response =>
        option match {
          case "tomorrow" => (response.json \ "DailyForecasts") (1)
          case _ => (response.json \ "DailyForecasts") (0)
        }
    }

    futureResponse
  }

  def current: Action[AnyContent] = Action.async {
    val city = config.get[String]("accuweather.city.code")
    val apiKey = config.get[String]("accuweather.api.key")

    val url = f"http://dataservice.accuweather.com/currentconditions/v1/$city?apikey=$apiKey&language=vi-vn"
    val request: WSRequest = ws.url(url)

    val futureResponse: Future[JsValue] = request.get().map {
      response => response.json(0)
    }

    futureResponse map { x =>
      val result: JsValue = Json.obj(
        "temp" -> (x \ "Temperature" \ "Metric" \ "Value").as[BigDecimal],
        "desc" -> (x \ "WeatherText").as[String]
      )
      Ok(result)
    }
  }

  def getCurrent: Future[JsValue] = {
    val city = config.get[String]("accuweather.city.code")
    val apiKey = config.get[String]("accuweather.api.key")

    val url = f"http://dataservice.accuweather.com/currentconditions/v1/$city?apikey=$apiKey&language=vi-vn"
    val request: WSRequest = ws.url(url)

    val futureResponse: Future[JsValue] = request.get().map {
      response => response.json(0)
    }

    futureResponse
  }

  def get(question: String): Action[AnyContent] = Action.async {
    val url = "https://lit-badlands-17614.herokuapp.com/classify"
    val request: WSRequest = ws.url(url)
      .withQueryStringParameters("q" -> question)

    val futureResponse: Future[JsValue] = request.get().map {
      response => response.json
    }

    futureResponse flatMap { x =>
      val main = (x \ "label" \ "main").as[String]

      main match {
        case "greeting" => Future(
          Ok(Json.obj("message" -> config.get[String]("response.greeting")))
        )
        case "weather" =>
          val sub = (x \ "label" \ "sub").as[String]

          sub match {
            case "now" =>
              val current = getCurrent

              current map { x =>
                val result: JsValue = Json.obj(
                  "min_temp" -> (x \ "Temperature" \ "Metric" \ "Value").as[BigDecimal],
                  "desc" -> (x \ "WeatherText").as[String],
                  "message" -> config.get[String]("response.weather.now")
                )
                Ok(result)
              }

            case "tomorrow" =>
              val forecast = getForecasts(sub)

              forecast map { x =>
                val result = Json.obj(
                  "max_temp" -> (x \ "Temperature" \ "Maximum" \ "Value").as[BigDecimal],
                  "min_temp" -> (x \ "Temperature" \ "Minimum" \ "Value").as[BigDecimal],
                  "desc" -> (x \ "Day" \ "IconPhrase").as[String],
                  "message" -> config.get[String]("response.weather.tomorrow")
                )

                Ok(result)
              }

            case _ =>
              val forecast = getForecasts(sub)

              forecast map { x =>
                val result = Json.obj(
                  "max_temp" -> (x \ "Temperature" \ "Maximum" \ "Value").as[BigDecimal],
                  "min_temp" -> (x \ "Temperature" \ "Minimum" \ "Value").as[BigDecimal],
                  "desc" -> (x \ "Day" \ "IconPhrase").as[String],
                  "message" -> config.get[String]("response.weather.today")
                )

                Ok(result)
              }
          }

        case _ => Future(
          Ok(Json.obj("message" -> config.get[String]("response.unknown")))
        )

      }
    }
  }

}
