package controllers.admin

import javax.inject.Inject
import play.api.mvc.{AbstractController, ControllerComponents}

class HomeController @Inject() (components: ControllerComponents) extends AbstractController(components){
  def index = Action {
    Ok("this is admin page")
  }

}
